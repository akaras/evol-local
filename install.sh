#!/bin/bash

export SD="../server-data"
export CONF="$SD/conf/import"

cp -f bin/char-server $SD
cp -f bin/login-server $SD
cp -f bin/map-server $SD
mkdir $SD/plugins
cp -f bin/plugins/*.so $SD/plugins

cd ../tools/localserver
./installconfigs.sh
